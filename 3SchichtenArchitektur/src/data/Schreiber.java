package data;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter; 
import java.io.IOException; 
import java.io.PrintWriter; 

public class Schreiber { 
	
	
    public static void schreiben(String text) { 
        PrintWriter pWriter = null; 
        try { 
            pWriter = new PrintWriter(new FileWriter("Promocode.txt", true), true); 
            pWriter.println(text); 
        } catch (IOException ioe) { 
            ioe.printStackTrace(); 
        } finally { 
            if (pWriter != null){ 
                pWriter.flush(); 
                pWriter.close(); 
            } 
        } 
    } 
} 