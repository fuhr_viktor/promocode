package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.SwingConstants;

import logic.IPromoCodeLogic;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PromocodeGUI extends JFrame {
	public PromocodeGUI(IPromoCodeLogic logic) {
		
		
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel label = new JLabel("");
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lbl_Überschrift = new JLabel("Promocode-Generierer");
		lbl_Überschrift.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Überschrift.setFont(new Font("Agency FB", Font.BOLD, 25));
		panel.add(lbl_Überschrift);
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.SOUTH);
		
		JButton btn_Beenden = new JButton("Exit");
		btn_Beenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panel_1.add(btn_Beenden);
		
		JPanel panel_4 = new JPanel();
		getContentPane().add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JLabel lbl_Promocode = new JLabel("");
		panel_4.add(lbl_Promocode, BorderLayout.CENTER);
		
		JButton btn_PromocodeGenerieren = new JButton("Promocode generieren");
		btn_PromocodeGenerieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String code = logic.getNewPromoCode();
				lbl_Promocode.setText(code);
			}
		});
		panel_1.add(btn_PromocodeGenerieren);
		
		
		this.setVisible(true);
	}

}



